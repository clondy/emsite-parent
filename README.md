#  **emsite-parent**  
### 框架简介

- emsite框架是众多是基于众多优秀的开源项目，高度整合封装而成的高效，高性能，强安全性的开源Java EE分布式全自动快速开发框架平台。
本项目参照jeesite、springside、jeesz等框架思想实现并从此不再兼容jeesite所有版本。
- emsite目前包括以下三大模块，系统管理（SYS）模块、内容管理（CMS）模块、代码生成（GEN）模块。系统管理模块，包括组织架构（用户
管理、机构管理、区域管理）、菜单管理、角色权限管理、字典管理等功能； 内容管理模块 ，包括内容管理（文章、链接），栏目管理、站点
管理、公共留言、文件管理、前端网站展示等功能；代码生成模块，完成重复的工作。
### 框架规划   
- emsite采用dubbo作为服务层框架，后台将集成单点登录、oauth2.0、storm+kafka消息处理系统、kafka+ flume+storm+hdfs+hadoop作
为日志分析系统、配置中心、分布式任务调度系统、服务器实时监控系统、搜索引擎系统(elasticsearch)。以上各大功能将作为模块化集成到
本项目，emipre团队后期将推出在线客服开源系统和统一微信公众号管理平台，敬请期待！

### 内置功能

    1、用户管理：用户是系统操作者，该功能主要完成系统用户配置。
    2、机构管理：配置系统组织机构（公司、部门、小组），树结构展现，可随意调整上下级。
    3、区域管理：系统城市区域模型，如：国家、省市、地市、区县的维护。
    4、菜单管理：配置系统菜单，操作权限，按钮权限标识等。
    5、角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
    6、字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：是否、男女、类别、级别等。
    7、操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
    8、连接池监视：监视当期系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
 
### 为何选择emsite

    1、使用 Apache License 2.0 协议，源代码完全开源，无商业限制。
    2、使用目前主流的Java EE开发框架，简单易学，学习成本低。
    3、数据库无限制，目前支持MySql、Oracle，可扩充SQL Server、PostgreSQL、H2等。
    4、模块化设计，层次结构清晰。内置一系列信息管理的基础功能。
    5、操作权限控制精密细致，对所有管理链接都进行权限验证，可控制到按钮。
    6、数据权限控制精密细致，对指定数据集权限进行过滤，七种数据权限可供选择。
    7、提供在线功能代码生成工具，提高开发效率及质量。
    8、提供常用工具类封装，日志、缓存、验证、字典、组织机构等，常用标签（taglib），获取当前组织机构、字典等数据。
    9、兼容目前最流行浏览器（IE7+、Chrome、Firefox）。
    10、最流行的分布式解决方案。
### 技术选型
1、后端

    a、核心框架：Spring Framework 4.1.9
    b、分布式服务框架：dubbo2.5.8
    c、分布式协调组件：zookeeper3.4.6
    d、安全框架：Apache Shiro 1.2
    e、视图框架：Spring MVC 4.1
    f、服务端验证：Hibernate Validator 5.2
    g、布局框架：SiteMesh 2.4
    h、任务调度：Spring Task 4.1
    i、持久层框架：MyBatis 3.2
    j、数据库连接池：Alibaba Druid 1.0
    k、缓存框架：Ehcache 2.6、Redis
    l、日志管理：SLF4J 1.7、Log4j
    m、工具类：Apache Commons、Jackson 2.2、Xstream 1.4、Dozer 5.3、POI 3.9
2、前端

    a、JS框架：jQuery 1.9。
    b、CSS框架：Twitter Bootstrap 2.3.1（稳定是后台，UI方面根据需求自己升级改造吧）。
    c、客户端验证：JQuery Validation Plugin 1.11。
    d、富文本在线编辑：CKEditor
    e、在线文件管理：CKFinder
    f、动态页签：Jerichotab
    g、手机端框架：Jingle
    h、数据表格：jqGrid
    i、对话框：jQuery jBox
    j、下拉选择框：jQuery Select2
    k、树结构控件：jQuery zTree
    l、日期控件： My97DatePicker
4、平台

    a、服务器中间件：在Java EE 5规范（Servlet 3.0、JSP 2.1）下开发，支持应用服务器中间件 有Tomcat 7+、Jboss 7+、
       WebLogic 10+、WebSphere 8+。
    b、数据库支持：目前仅提供MySql或Oracle数据库的支持，但不限于数据库，平台留有其它数据库支持接口， 你可以很方便
       的更改为其它数据库，如：SqlServer 2008、MySql 5.5、H2等
    c、开发环境：Jdk1.7、（Eclipse Java EE 4.3|sts-3.6.2.RELEASE|myeclipse10）、Maven 3.2、Git
### 安全考虑

    a、开发语言：系统采用Java 语言开发，具有卓越的通用性、高效性、平台移植性和安全性。
    b、分层设计：（数据库层，数据访问层，业务逻辑层，展示层）层次清楚，低耦合，各层必须通过接口才能接入并进行参数校
       验（如：在展示层不可直接操作数据库），保证数据操作的安全。
    c、双重验证：用户表单提交双验证：包括服务器端验证及客户端验证，防止用户通过浏览器恶意修改（如不可写文本域、隐藏
       变量篡改、上传非法文件等），跳过客户端验证操作数据库。
    d、安全编码：用户表单提交所有数据，在服务器端都进行安全编码，防止用户提交非法脚本及SQL注入获取敏感数据等，确保数据安全。
    e、密码加密：登录用户密码进行SHA1散列加密，此加密方法是不可逆的。保证密文泄露后的安全问题。
    f、强制访问：系统对所有管理端链接都进行用户身份权限验证，防止用户
    g、服务监控：系统采用dubbo的服务全链路监控技术、可以做到精准定位服务情况
### 快速体验

    a、具备运行环境：JDK1.7+、Maven3.0+、MySql5+或Oracle10g+。
    b、下载emsite-parent项目，导入第三方ckfinder插件jar包到本地maven库
    c、修改src\main\resources\emsite.properties文件中的数据库设置参数。
    d、根据修改参数创建对应MySql或Oracle数据库用户和参数。
    e、运行bin\init-db.bat脚本，即可导入表结构及演示数据(linux操作系统：在控制台中切换至项目根目录，运行命令：mvn antrun:run -Pinit-
       db);或者直接在数据库工具中运行emsite web项目bin目录下的emsite_mysql.sql文件。
    f、启动redis,zookeeper组件
    g、运行bin\run-tomcat7.bat或bin\run-jetty.bat，启动Web服务器（第一次运行，需要下载依赖jar包，请耐心等待）。
    h、最高管理员账号，用户名：emsite 密码：admin  
### 常见问题

    a、用一段时间提示内存溢出，请修改JVM参数：-Xmx512m -XX:MaxPermSize=256m
    b、有时出现文字乱码：修改Tomcat的server.xml文件的Connector项，增加URIEncoding="UTF-8"
    c、为什么新建菜单后看不到新建的菜单？因为授权问题，菜单管理只允许最高管理员账号管理（最高管理员默认账号：emsite密码：admin）。 
    d、第三方ckfinder导入方案
       mvn install:install-file -DgroupId=com.ckfinder -DartifactId=ckfinder   -Dversion=2.3 -Dpackaging=jar -        
       Dfile=D:/thirdxsd/ckfinder-2.3.jar

       mvn install:install-file -DgroupId=com.ckfinder -DartifactId=ckfinderplugin-fileeditor -Dversion=2.3 -Dpackaging=jar -
       Dfile=D:/thirdxsd/ckfinderplugin-fileeditor-2.3.jar

       mvn install:install-file -DgroupId=com.ckfinder -DartifactId=ckfinderplugin-imageresize -Dversion=2.3 -Dpackaging=jar -    
       Dfile=D:/thirdxsd/ckfinderplugin-imageresize-2.3.jar

    e、maven报错Project configuration is not up-to-date with pom.xml错误解决方法(导入项目之后发现有一个如下的错误：Project     
       configuration is not up-to-date with pom.xml. Run project configuration)    
       update
       其实这个问题解决非常简单：
       在项目上右键——【Maven】——【Update Project Configuration……】
       这时会打开一个（Update Maven Dependencies）的对话框，然后勾选住出错的项目，点击Ok，这样就搞定了。

### 项目结构如下：
![输入图片说明](https://gitee.com/uploads/images/2018/0202/143256_f34cdfd3_302505.png "(F(P2CK]Q@]VAZA3M2~7R54.png")
### 运行效果： 
![输入图片说明](https://gitee.com/uploads/images/2018/0202/143744_7016b877_302505.png "QL_HX_CFH3XM9}}2O$4E5V9.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0202/143757_4a2f2fbe_302505.png "{XI07R%C`~Z3]QW~W@@GV7R.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0202/143805_b21e58a0_302505.png "BFQ4DY62930{X$`}B8CTX(W.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0202/143814_657153ba_302505.png "A{VU)DC0ME4@T36M}(OV3VQ.png")
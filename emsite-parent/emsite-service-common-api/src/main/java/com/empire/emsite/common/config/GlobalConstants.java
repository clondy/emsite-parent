/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.config;

//import com.ckfinder.connector.ServletContextFactory;

/**
 * 类Global.java的实现描述：全局配置类
 * 
 * @author arron 2017年10月30日 下午3:51:28
 */
public interface GlobalConstants {
    public static final String DBNAME = "mysql";

    /**
     * 枚举类型【显示/隐藏】
     */
    enum ShowHideEnum {
        SHOW("1", "显示"),
        HIDE("0", "隐藏");
        /**
         * 值
         */
        private String value;
        /**
         * 名称
         */
        private String name;

        private ShowHideEnum(String value, String name) {
            this.name = name;
            this.value = value;
        }

        /**
         * 根据枚举值获得枚举对象
         * 
         * @param value
         * @return
         */
        public static ShowHideEnum getShowHideEnumByValue(String value) {
            for (ShowHideEnum temp : ShowHideEnum.values()) {
                if (temp.getValue().equals(value))
                    return temp;
            }
            return null;
        }

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * 枚举类型【是/否】
     */
    enum YesNoEnum {
        YES("1", "是"),
        NO("0", "否");

        private String value;
        private String name;

        private YesNoEnum(String value, String name) {
            this.name = name;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }

    }

    /**
     * 枚举类型【是/否】
     */
    enum TrueFalseEnum {
        TRUE("true", "是"),
        FALSE("false", "否");
        private String value;
        private String name;

        private TrueFalseEnum(String value, String name) {
            this.name = name;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }
    }
}

/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Comment;
import com.empire.emsite.modules.cms.facade.CommentFacadeService;
import com.empire.emsite.modules.cms.service.CommentService;

/**
 * 类CommentFacadeServiceImpl.java的实现描述：评论FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:45:16
 */
@Service
public class CommentFacadeServiceImpl implements CommentFacadeService {
    @Autowired
    private CommentService commentService;

    @Override
    public Comment get(String id) {
        return commentService.get(id);
    }

    @Override
    public Page<Comment> findPage(Page<Comment> page, Comment comment) {
        return commentService.findPage(page, comment);
    }

    @Override
    public void save(Comment comment) {
        commentService.save(comment);
    }

    @Override
    public void delete(Comment entity, Boolean isRe) {
        commentService.delete(entity, isRe);
    }
}

/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.test.entity.TestData;
import com.empire.emsite.test.facade.TestDataFacadeService;
import com.empire.emsite.test.service.TestDataService;

/**
 * 类TestDataFacadeServiceImpl.java的实现描述：单表生成FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:59:30
 */
@Service
public class TestDataFacadeServiceImpl implements TestDataFacadeService {

    @Autowired
    private TestDataService testDataService;

    @Override
    public List<TestData> findList(TestData testData) {
        return testDataService.findList(testData);
    }

    @Override
    public Page<TestData> findPage(Page<TestData> page, TestData testData) {
        return testDataService.findPage(page, testData);
    }

    @Override
    public TestData get(String id) {
        return testDataService.get(id);
    }

    @Override
    public void save(TestData testData) {
        testDataService.save(testData);
    }

    @Override
    public void delete(TestData testData) {
        testDataService.delete(testData);
    }
}

/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.service.SiteService;

/**
 * 类SiteFacadeServiceImpl.java的实现描述：站点FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:45:59
 */
@Service
public class SiteFacadeServiceImpl implements SiteFacadeService {
    @Autowired
    private SiteService siteService;

    @Override
    public Site get(String id) {
        return siteService.get(id);
    }

    @Override
    public Page<Site> findPage(Page<Site> page, Site site) {
        return siteService.findPage(page, site);
    }

    @Override
    public void save(Site site) {
        siteService.save(site);
    }

    @Override
    public void delete(Site site, Boolean isRe) {
        siteService.delete(site, isRe);
    }
}

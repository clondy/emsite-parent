/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Guestbook;
import com.empire.emsite.modules.cms.facade.GuestbookFacadeService;
import com.empire.emsite.modules.cms.service.GuestbookService;

/**
 * 类GuestbookFacadeServiceImpl.java的实现描述： 留言FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:45:35
 */
@Service
public class GuestbookFacadeServiceImpl implements GuestbookFacadeService {
    @Autowired
    private GuestbookService guestbookService;

    @Override
    public Guestbook get(String id) {
        return guestbookService.get(id);
    }

    @Override
    public Page<Guestbook> findPage(Page<Guestbook> page, Guestbook guestbook) {
        return guestbookService.findPage(page, guestbook);
    }

    @Override
    public void delete(Guestbook guestbook, Boolean isRe) {
        guestbookService.delete(guestbook, isRe);
    }

    /**
     * 更新索引
     */
    @Override
    public void createIndex() {
        guestbookService.createIndex();
    }

    /**
     * 全文检索
     */
    //FIXME 暂不提供
    @Override
    public Page<Guestbook> search(Page<Guestbook> page, String q, String beginDate, String endDate) {
        return guestbookService.search(page, q, beginDate, endDate);
    }

    @Override
    public void save(Guestbook guestbook) {
        guestbookService.save(guestbook);
    }
}

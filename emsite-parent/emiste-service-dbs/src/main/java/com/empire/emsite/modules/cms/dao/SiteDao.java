/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.dao;

import com.empire.emsite.common.persistence.CrudDao;
import com.empire.emsite.common.persistence.annotation.MyBatisDao;
import com.empire.emsite.modules.cms.entity.Site;

/**
 * 类SiteDao.java的实现描述：站点DAO接口
 * 
 * @author arron 2017年10月30日 下午4:08:23
 */
@MyBatisDao
public interface SiteDao extends CrudDao<Site> {

}

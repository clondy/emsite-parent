/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empire.emsite.common.service.CrudService;
import com.empire.emsite.modules.cms.dao.ArticleDataDao;
import com.empire.emsite.modules.cms.entity.ArticleData;

/**
 * 类ArticleDataService.java的实现描述：站点Service
 * 
 * @author arron 2017年10月30日 下午4:09:40
 */
@Service
@Transactional(readOnly = true)
public class ArticleDataService extends CrudService<ArticleDataDao, ArticleData> {

}

/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.empire.emsite.test.entity.TestDataChild;
import com.empire.emsite.test.entity.TestDataMain;
import com.empire.emsite.test.facade.TestDataMainFacadeService;

/**
 * 类TestDataMainController.java的实现描述：主子表生成Controller
 * 
 * @author arron 2017年10月30日 下午7:26:05
 */
@Controller
@RequestMapping(value = "${adminPath}/test/testDataMain")
public class TestDataMainController extends BaseController {

    @Autowired
    private TestDataMainFacadeService testDataMainFacadeService;

    @ModelAttribute
    public TestDataMain get(@RequestParam(required = false) String id) {
        TestDataMain entity = null;
        if (StringUtils.isNotBlank(id)) {
            entity = testDataMainFacadeService.get(id);
        }
        if (entity == null) {
            entity = new TestDataMain();
        }
        return entity;
    }

    @RequiresPermissions("test:testDataMain:view")
    @RequestMapping(value = { "list", "" })
    public String list(TestDataMain testDataMain, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<TestDataMain> page = testDataMainFacadeService.findPage(new Page<TestDataMain>(request, response),
                testDataMain);
        model.addAttribute("page", page);
        return "emsite/test/testDataMainList";
    }

    @RequiresPermissions("test:testDataMain:view")
    @RequestMapping(value = "form")
    public String form(TestDataMain testDataMain, Model model) {
        model.addAttribute("testDataMain", testDataMain);
        return "emsite/test/testDataMainForm";
    }

    @RequiresPermissions("test:testDataMain:edit")
    @RequestMapping(value = "save")
    public String save(TestDataMain testDataMain, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, testDataMain)) {
            return form(testDataMain, model);
        }
        for (TestDataChild testDataChild : testDataMain.getTestDataChildList()) {
            testDataChild.setCurrentUser(UserUtils.getUser());
        }
        testDataMain.setCurrentUser(UserUtils.getUser());
        testDataMainFacadeService.save(testDataMain);
        addMessage(redirectAttributes, "保存主子表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testDataMain/?repage";
    }

    @RequiresPermissions("test:testDataMain:edit")
    @RequestMapping(value = "delete")
    public String delete(TestDataMain testDataMain, RedirectAttributes redirectAttributes) {
        testDataMainFacadeService.delete(testDataMain);
        addMessage(redirectAttributes, "删除主子表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testDataMain/?repage";
    }

}

/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web.front;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.servlet.ValidateCodeServlet;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Article;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.entity.Comment;
import com.empire.emsite.modules.cms.entity.Link;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.ArticleDataFacadeService;
import com.empire.emsite.modules.cms.facade.ArticleFacadeService;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.facade.CommentFacadeService;
import com.empire.emsite.modules.cms.facade.LinkFacadeService;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.google.common.collect.Lists;

/**
 * 类FrontController.java的实现描述：网站Controller
 * 
 * @author arron 2017年10月30日 下午7:14:19
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class FrontController extends BaseController {

    @Autowired
    private ArticleFacadeService     articleFacadeService;
    @Autowired
    private ArticleDataFacadeService articleDataFacadeService;
    @Autowired
    private LinkFacadeService        linkFacadeService;
    @Autowired
    private CommentFacadeService     commentFacadeService;
    @Autowired
    private CategoryFacadeService    categoryFacadeService;
    @Autowired
    private SiteFacadeService        siteFacadeService;

    /**
     * 网站首页
     */
    @RequestMapping
    public String index(Model model) {
        Site site = CmsUtils.getSite(Site.defaultSiteId());
        model.addAttribute("site", site);
        model.addAttribute("isIndex", true);
        return "modules/cms/front/themes/" + site.getTheme() + "/frontIndex";
    }

    /**
     * 网站首页
     */
    @RequestMapping(value = "index-{siteId}${urlSuffix}")
    public String index(@PathVariable String siteId, Model model) {
        if (siteId.equals("1")) {
            return "redirect:" + MainConfManager.getFrontPath();
        }
        Site site = CmsUtils.getSite(siteId);
        // 子站有独立页面，则显示独立页面
        if (StringUtils.isNotBlank(site.getCustomIndexView())) {
            model.addAttribute("site", site);
            model.addAttribute("isIndex", true);
            return "modules/cms/front/themes/" + site.getTheme() + "/frontIndex" + site.getCustomIndexView();
        }
        // 否则显示子站第一个栏目
        List<Category> mainNavList = CmsUtils.getMainNavList(siteId);
        if (mainNavList.size() > 0) {
            String firstCategoryId = CmsUtils.getMainNavList(siteId).get(0).getId();
            return "redirect:" + MainConfManager.getFrontPath() + "/list-" + firstCategoryId
                    + MainConfManager.getUrlSuffix();
        } else {
            model.addAttribute("site", site);
            return "modules/cms/front/themes/" + site.getTheme() + "/frontListCategory";
        }
    }

    /**
     * 内容列表
     */
    @RequestMapping(value = "list-{categoryId}${urlSuffix}")
    public String list(@PathVariable String categoryId,
                       @RequestParam(required = false, defaultValue = "1") Integer pageNo,
                       @RequestParam(required = false, defaultValue = "15") Integer pageSize, Model model) {
        Category category = categoryFacadeService.get(categoryId);
        if (category == null) {
            Site site = CmsUtils.getSite(Site.defaultSiteId());
            model.addAttribute("site", site);
            return "error/404";
        }
        Site site = siteFacadeService.get(category.getSite().getId());
        model.addAttribute("site", site);
        // 2：简介类栏目，栏目第一条内容
        if ("2".equals(category.getShowModes()) && "article".equals(category.getModule())) {
            // 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
            List<Category> categoryList = Lists.newArrayList();
            if (category.getParent().getId().equals("1")) {
                categoryList.add(category);
            } else {
                categoryList = categoryFacadeService.findByParentId(category.getParent().getId(), category.getSite()
                        .getId());
            }
            model.addAttribute("category", category);
            model.addAttribute("categoryList", categoryList);
            // 获取文章内容
            Page<Article> page = new Page<Article>(1, 1, -1);
            Article article = new Article(category);
            page = articleFacadeService.findPage(page, article, false);
            if (page.getList().size() > 0) {
                article = page.getList().get(0);
                article.setArticleData(articleDataFacadeService.get(article.getId()));
                articleFacadeService.updateHitsAddOne(article.getId());
            }
            model.addAttribute("article", article);
            CmsUtils.addViewConfigAttribute(model, category);
            CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
            return "modules/cms/front/themes/" + site.getTheme() + "/" + getTpl(article);
        } else {
            List<Category> categoryList = categoryFacadeService.findByParentId(category.getId(), category.getSite()
                    .getId());
            // 展现方式为1 、无子栏目或公共模型，显示栏目内容列表
            if ("1".equals(category.getShowModes()) || categoryList.size() == 0) {
                // 有子栏目并展现方式为1，则获取第一个子栏目；无子栏目，则获取同级分类列表。
                if (categoryList.size() > 0) {
                    category = categoryList.get(0);
                } else {
                    // 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
                    if (category.getParent().getId().equals("1")) {
                        categoryList.add(category);
                    } else {
                        categoryList = categoryFacadeService.findByParentId(category.getParent().getId(), category
                                .getSite().getId());
                    }
                }
                model.addAttribute("category", category);
                model.addAttribute("categoryList", categoryList);
                // 获取内容列表
                if ("article".equals(category.getModule())) {
                    Page<Article> page = new Page<Article>(pageNo, pageSize);
                    //System.out.println(page.getPageNo());
                    page = articleFacadeService.findPage(page, new Article(category), false);
                    model.addAttribute("page", page);
                    // 如果第一个子栏目为简介类栏目，则获取该栏目第一篇文章
                    if ("2".equals(category.getShowModes())) {
                        Article article = new Article(category);
                        if (page.getList().size() > 0) {
                            article = page.getList().get(0);
                            article.setArticleData(articleDataFacadeService.get(article.getId()));
                            articleFacadeService.updateHitsAddOne(article.getId());
                        }
                        model.addAttribute("article", article);
                        CmsUtils.addViewConfigAttribute(model, category);
                        CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
                        return "modules/cms/front/themes/" + site.getTheme() + "/" + getTpl(article);
                    }
                } else if ("link".equals(category.getModule())) {
                    Page<Link> page = new Page<Link>(1, -1);
                    Link currLink = new Link(category);
                    currLink.setCurrentUser(UserUtils.getUser());
                    page = linkFacadeService.findPage(page, currLink, false);
                    model.addAttribute("page", page);
                }
                String view = "/frontList";
                if (StringUtils.isNotBlank(category.getCustomListView())) {
                    view = "/" + category.getCustomListView();
                }
                CmsUtils.addViewConfigAttribute(model, category);
                site = siteFacadeService.get(category.getSite().getId());
                //System.out.println("else 栏目第一条内容 _2 :"+category.getSite().getTheme()+","+site.getTheme());
                return "modules/cms/front/themes/" + siteFacadeService.get(category.getSite().getId()).getTheme()
                        + view;
                //return "modules/cms/front/themes/"+category.getSite().getTheme()+view;
            }
            // 有子栏目：显示子栏目列表
            else {
                model.addAttribute("category", category);
                model.addAttribute("categoryList", categoryList);
                String view = "/frontListCategory";
                if (StringUtils.isNotBlank(category.getCustomListView())) {
                    view = "/" + category.getCustomListView();
                }
                CmsUtils.addViewConfigAttribute(model, category);
                return "modules/cms/front/themes/" + site.getTheme() + view;
            }
        }
    }

    /**
     * 内容列表（通过url自定义视图）
     */
    @RequestMapping(value = "listc-{categoryId}-{customView}${urlSuffix}")
    public String listCustom(@PathVariable String categoryId, @PathVariable String customView,
                             @RequestParam(required = false, defaultValue = "1") Integer pageNo,
                             @RequestParam(required = false, defaultValue = "15") Integer pageSize, Model model) {
        Category category = categoryFacadeService.get(categoryId);
        if (category == null) {
            Site site = CmsUtils.getSite(Site.defaultSiteId());
            model.addAttribute("site", site);
            return "error/404";
        }
        Site site = siteFacadeService.get(category.getSite().getId());
        model.addAttribute("site", site);
        List<Category> categoryList = categoryFacadeService
                .findByParentId(category.getId(), category.getSite().getId());
        model.addAttribute("category", category);
        model.addAttribute("categoryList", categoryList);
        CmsUtils.addViewConfigAttribute(model, category);
        return "modules/cms/front/themes/" + site.getTheme() + "/frontListCategory" + customView;
    }

    /**
     * 显示内容
     */
    @RequestMapping(value = "view-{categoryId}-{contentId}${urlSuffix}")
    public String view(@PathVariable String categoryId, @PathVariable String contentId, Model model) {
        Category category = categoryFacadeService.get(categoryId);
        if (category == null) {
            Site site = CmsUtils.getSite(Site.defaultSiteId());
            model.addAttribute("site", site);
            return "error/404";
        }
        model.addAttribute("site", category.getSite());
        if ("article".equals(category.getModule())) {
            // 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
            List<Category> categoryList = Lists.newArrayList();
            if (category.getParent().getId().equals("1")) {
                categoryList.add(category);
            } else {
                categoryList = categoryFacadeService.findByParentId(category.getParent().getId(), category.getSite()
                        .getId());
            }
            // 获取文章内容
            Article article = articleFacadeService.get(contentId);
            if (article == null || !Article.DEL_FLAG_NORMAL.equals(article.getDelFlag())) {
                return "error/404";
            }
            // 文章阅读次数+1
            articleFacadeService.updateHitsAddOne(contentId);
            // 获取推荐文章列表
            List<Object[]> relationList = articleFacadeService.findByIds(articleDataFacadeService.get(article.getId())
                    .getRelation());
            // 将数据传递到视图
            model.addAttribute("category", categoryFacadeService.get(article.getCategory().getId()));
            model.addAttribute("categoryList", categoryList);
            article.setArticleData(articleDataFacadeService.get(article.getId()));
            model.addAttribute("article", article);
            model.addAttribute("relationList", relationList);
            CmsUtils.addViewConfigAttribute(model, article.getCategory());
            CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
            Site site = siteFacadeService.get(category.getSite().getId());
            model.addAttribute("site", site);
            //			return "modules/cms/front/themes/"+category.getSite().getTheme()+"/"+getTpl(article);
            return "modules/cms/front/themes/" + site.getTheme() + "/" + getTpl(article);
        }
        return "error/404";
    }

    /**
     * 内容评论
     */
    @RequestMapping(value = "comment", method = RequestMethod.GET)
    public String comment(String theme, Comment comment, HttpServletRequest request, HttpServletResponse response,
                          Model model) {
        Page<Comment> page = new Page<Comment>(request, response);
        Comment c = new Comment();
        c.setCategory(comment.getCategory());
        c.setContentId(comment.getContentId());
        c.setDelFlag(Comment.DEL_FLAG_NORMAL);
        c.setCurrentUser(UserUtils.getUser());
        page = commentFacadeService.findPage(page, c);
        model.addAttribute("page", page);
        model.addAttribute("comment", comment);
        return "modules/cms/front/themes/" + theme + "/frontComment";
    }

    /**
     * 内容评论保存
     */
    @ResponseBody
    @RequestMapping(value = "comment", method = RequestMethod.POST)
    public String commentSave(Comment comment, String validateCode, @RequestParam(required = false) String replyId,
                              HttpServletRequest request) {
        if (StringUtils.isNotBlank(validateCode)) {
            if (ValidateCodeServlet.validate(request, validateCode)) {
                if (StringUtils.isNotBlank(replyId)) {
                    Comment replyComment = commentFacadeService.get(replyId);
                    if (replyComment != null) {
                        comment.setContent("<div class=\"reply\">" + replyComment.getName() + ":<br/>"
                                + replyComment.getContent() + "</div>" + comment.getContent());
                    }
                }
                comment.setIp(request.getRemoteAddr());
                comment.setCreateDate(new Date());
                comment.setDelFlag(Comment.DEL_FLAG_AUDIT);
                comment.setCurrentUser(UserUtils.getUser());
                commentFacadeService.save(comment);
                return "{result:1, message:'提交成功。'}";
            } else {
                return "{result:2, message:'验证码不正确。'}";
            }
        } else {
            return "{result:2, message:'验证码不能为空。'}";
        }
    }

    /**
     * 站点地图
     */
    @RequestMapping(value = "map-{siteId}${urlSuffix}")
    public String map(@PathVariable String siteId, Model model) {
        Site site = CmsUtils.getSite(siteId != null ? siteId : Site.defaultSiteId());
        model.addAttribute("site", site);
        return "modules/cms/front/themes/" + site.getTheme() + "/frontMap";
    }

    private String getTpl(Article article) {
        if (StringUtils.isBlank(article.getCustomContentView())) {
            String view = null;
            Category c = article.getCategory();
            boolean goon = true;
            do {
                if (StringUtils.isNotBlank(c.getCustomContentView())) {
                    view = c.getCustomContentView();
                    goon = false;
                } else if (c.getParent() == null || c.getParent().isRoot()) {
                    goon = false;
                } else {
                    c = c.getParent();
                }
            } while (goon);
            return StringUtils.isBlank(view) ? Article.DEFAULT_TEMPLATE : view;
        } else {
            return article.getCustomContentView();
        }
    }

}

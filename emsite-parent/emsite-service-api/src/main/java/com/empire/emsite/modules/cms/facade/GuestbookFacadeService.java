/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Guestbook;

/**
 * 类GuestbookFacadeService.java的实现描述：留言FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:43:35
 */
public interface GuestbookFacadeService {

    public Guestbook get(String id);

    public void save(Guestbook guestbook);

    public Page<Guestbook> findPage(Page<Guestbook> page, Guestbook guestbook);

    public void delete(Guestbook guestbook, Boolean isRe);

    /**
     * 更新索引
     */
    public void createIndex();

    /**
     * 全文检索
     */
    //FIXME 暂不提供
    public Page<Guestbook> search(Page<Guestbook> page, String q, String beginDate, String endDate);
}

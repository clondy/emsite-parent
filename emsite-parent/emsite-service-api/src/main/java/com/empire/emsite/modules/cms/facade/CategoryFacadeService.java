/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.sys.entity.User;

/**
 * 类CategoryFacadeService.java的实现描述：栏目FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:42:44
 */
public interface CategoryFacadeService {
    public Category get(String id);

    public List<Category> findByUser(boolean isCurrentSite, String module, User user, String currSiteId);

    public List<Category> findByParentId(String parentId, String siteId);

    public Page<Category> find(Page<Category> page, Category category);

    public void save(Category category);

    public void delete(Category category);

    /**
     * 通过编号获取栏目列表
     */
    public List<Category> findByIds(String ids);
}

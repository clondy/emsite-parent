/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Comment;

/**
 * 类CommentFacadeService.java的实现描述：评论FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:43:02
 */
public interface CommentFacadeService {
    public Comment get(String id);

    public Page<Comment> findPage(Page<Comment> page, Comment comment);

    public void save(Comment comment);

    public void delete(Comment entity, Boolean isRe);
}
